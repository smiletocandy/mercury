package com.tamguo.mercury.config.dao;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;

/**
 * 实体父类
 */
public class SuperEntity<T extends Model<?>> extends Model<T> {

	private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @Override
    protected Serializable pkVal() {
        return this.getId();
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    
}
