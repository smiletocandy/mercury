package com.tamguo.mercury.controller;

import com.tamguo.mercury.utils.ShiroUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogoutController {

	@RequestMapping(path="logout")
	public ModelAndView logout(ModelAndView model) {
		ShiroUtils.logout();
		model.setViewName("login");
		return model;
	}
	
}
