package com.tamguo.mercury.controller;

import com.tamguo.mercury.utils.CaptchaUtils;
import com.tamguo.mercury.utils.ExceptionSupport;
import com.tamguo.mercury.utils.ShiroUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ValidCodeController {

	/** 验证码常数*/
	public static final String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";

	@RequestMapping("validCode")
	public void validCode(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");
		
		String a = CaptchaUtils.generateCaptcha(response.getOutputStream());
		ShiroUtils.setSessionAttribute(KAPTCHA_SESSION_KEY, a);
	}
	
	@RequestMapping("checkCode")
	@ResponseBody
	public Boolean checkCode(String validCode) throws ServletException, IOException {
		try {
			String kaptcha = ShiroUtils.getKaptcha(KAPTCHA_SESSION_KEY);
			if (validCode.equalsIgnoreCase(kaptcha)) {
				return true;
			}
		} catch (Exception e) {
			ExceptionSupport.resolverResult("验证编码错误", this.getClass(), e);
		}
		return false;
	}
	
}
