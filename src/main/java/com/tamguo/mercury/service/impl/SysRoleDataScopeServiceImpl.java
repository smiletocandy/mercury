package com.tamguo.mercury.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tamguo.mercury.dao.SysRoleDataScopeMapper;
import com.tamguo.mercury.model.SysRoleDataScopeEntity;
import com.tamguo.mercury.service.ISysRoleDataScopeService;
import org.springframework.stereotype.Service;

@Service
public class SysRoleDataScopeServiceImpl extends ServiceImpl<SysRoleDataScopeMapper, SysRoleDataScopeEntity> implements ISysRoleDataScopeService {

	
}
