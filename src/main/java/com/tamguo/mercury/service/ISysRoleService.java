package com.tamguo.mercury.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysRoleEntity;
import com.tamguo.mercury.model.condition.SysRoleCondition;

import java.util.Map;
/**
 * @author tanguo
 */
public interface ISysRoleService extends IService<SysRoleEntity> {

	/** 列表查询*/
	Page<SysRoleEntity> listData(SysRoleCondition condition);

	/** 属性菜单*/
	Map<String, Object> menuTreeData(String roleCode);

	/** 分配功能权限*/
	void allowMenuPermission(SysRoleEntity role);

	/** 授权数据权限*/
	void allowDataScope(SysRoleEntity role);

	/** 角色树形结构*/
	JSONArray treeDate(String userType);

	/** 修改角色*/
	void update(SysRoleEntity role);

	/** 新增角色*/
	void save(SysRoleEntity role);

}
