package com.tamguo.mercury.service;

import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysRoleDataScopeEntity;
/**
 * @author tanguo
 */
public interface ISysRoleDataScopeService extends IService<SysRoleDataScopeEntity> {

}
