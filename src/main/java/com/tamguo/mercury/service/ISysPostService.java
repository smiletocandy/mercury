package com.tamguo.mercury.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysPostEntity;
import com.tamguo.mercury.model.condition.SysPostCondition;

/**
 * @author tanguo
 */
public interface ISysPostService extends IService<SysPostEntity>{

	/** 查询列表*/
	Page<SysPostEntity> listData(SysPostCondition condition);

	/** 添加数据*/
	void add(SysPostEntity post);

	/** 修改数据*/
	void update(SysPostEntity post);

}
