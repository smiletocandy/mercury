package com.tamguo.mercury.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysMenuEntity;
import com.tamguo.mercury.model.condition.SysMenuCondition;
import java.util.List;

public interface ISysMenuService extends IService<SysMenuEntity>{

	/** 列表数据*/
	List<SysMenuEntity> listData(SysMenuCondition condition);

	/** 树形结构*/
	JSONArray treeData(String excludeId);

	/** 新增菜单*/
	void save(SysMenuEntity menu);

	/** 修改菜单*/
	void update(SysMenuEntity menu);
	
}
