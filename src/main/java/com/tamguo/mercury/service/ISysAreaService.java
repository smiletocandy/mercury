package com.tamguo.mercury.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysAreaEntity;
import com.tamguo.mercury.model.condition.SysAreaCondition;
import com.tamguo.mercury.utils.Result;
import java.util.List;

/**
 * @author tanguo
 */
public interface ISysAreaService extends IService<SysAreaEntity>{

	List<SysAreaEntity> listData(SysAreaCondition condition);

	JSONArray treeData(String excludeId);

	/** 保存地区*/
	void save(SysAreaEntity area);

	/** 修改地区*/
	void update(SysAreaEntity area);
	
	/** 树形*/
	Result findAreaTree();

}
