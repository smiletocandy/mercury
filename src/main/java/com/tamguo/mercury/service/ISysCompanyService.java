package com.tamguo.mercury.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysCompanyEntity;
import com.tamguo.mercury.model.condition.SysCompanyCondition;

import java.util.List;

public interface ISysCompanyService extends IService<SysCompanyEntity>{
	
	/** 公司树形结构*/
	JSONArray treeData(String excludeId);

	/** 查询公司列表*/
	List<SysCompanyEntity> listData(SysCompanyCondition condition);
	
	/** 根据ID查询公司*/
	SysCompanyEntity selectByCode(String code);

	/** 新建公司*/
	void save(SysCompanyEntity company);

	/** 修改公司*/
	void update(SysCompanyEntity company);
}
