package com.tamguo.mercury.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.service.IService;
import com.tamguo.mercury.model.SysOfficeEntity;
import com.tamguo.mercury.model.condition.SysOfficeCondition;
import java.util.List;

public interface ISysOfficeService extends IService<SysOfficeEntity>{

	List<SysOfficeEntity> listData(SysOfficeCondition condition);

	JSONArray treeData(String excludeId);

	void save(SysOfficeEntity office);

	void update(SysOfficeEntity office);

}
