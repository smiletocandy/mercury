package com.tamguo.mercury.model.enums;

import com.baomidou.mybatisplus.enums.IEnum;

import java.io.Serializable;

/**
 * 用户状态
 */
public enum SysOfficeTypeEnum implements IEnum {
	PROVINCE("province", "省级公司"),
	CITY("city", "市级公司"),
	DEPARTMENT("department" , "部门");

    private String value;
    private String desc;

    SysOfficeTypeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Serializable getValue() {
        return this.value;
    }

    public String getDesc(){
        return this.desc;
    }
    
    @Override
    public String toString() {
    	return this.value;
    }
}
