package com.tamguo.mercury.model.enums;

import com.baomidou.mybatisplus.enums.IEnum;

import java.io.Serializable;

/**
 * 用户状态
 */
public enum SysUserTypeEnum implements IEnum {
	EMPLOYEE("employee", "雇员"),
	NONE("none", "系统");

    private String value;
    private String desc;

    SysUserTypeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Serializable getValue() {
        return this.value;
    }

    public String getDesc(){
        return this.desc;
    }
    
    @Override
    public String toString() {
    	return this.value;
    }
}
