package com.tamguo.mercury.model.enums;

import com.baomidou.mybatisplus.enums.IEnum;

import java.io.Serializable;

/**
 * 用户状态
 */
public enum SysPostStatusEnum implements IEnum {
	NORMAL("normal", "正常"),
	DISABLED("disable" , "停用");

    private String value;
    private String desc;

    SysPostStatusEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Serializable getValue() {
        return this.value;
    }

    public String getDesc(){
        return this.desc;
    }
    
    @Override
    public String toString() {
    	return this.value;
    }
}
