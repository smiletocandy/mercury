package com.tamguo.mercury.model;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * @author tanguo
 */
@Data
@TableName(value="sys_user_role")
public class SysUserRoleEntity {

	private String userCode;
	private String roleCode;

}
