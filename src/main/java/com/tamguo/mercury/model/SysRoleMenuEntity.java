package com.tamguo.mercury.model;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * @author tanguo
 */
@Data
@TableName(value="sys_role_menu")
public class SysRoleMenuEntity {

	private String roleCode;
	private String menuCode;

}
