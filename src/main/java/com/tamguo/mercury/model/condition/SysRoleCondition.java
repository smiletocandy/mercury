package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysRoleCondition {
	
	private Integer pageNo;
	private Integer pageSize;


}
