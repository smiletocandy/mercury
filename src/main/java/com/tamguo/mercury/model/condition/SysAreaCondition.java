package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysAreaCondition {

	private String parentCode;
	
	private Integer pageNo;
	private Integer pageSize;

}
