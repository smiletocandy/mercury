package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysOfficeCondition {

	private String parentCode;

}
