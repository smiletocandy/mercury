package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysPostCondition {
	
	private String code;
	private String name;
	private String postType;
	private String status;
	private Integer pageNo;
	private Integer pageSize;
}
