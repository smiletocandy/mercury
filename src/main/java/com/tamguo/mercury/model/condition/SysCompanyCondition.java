package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysCompanyCondition {

	private String parentCode;
	private String companyName;
	private String fullName;
}
