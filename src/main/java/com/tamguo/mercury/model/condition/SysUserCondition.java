package com.tamguo.mercury.model.condition;

import lombok.Data;

/**
 * @author tanguo
 */
@Data
public class SysUserCondition {

	private Integer pageNo;
	private Integer pageSize;
	
	private String loginCode;
	private String userName;
	private String email;
	private String mobile;
	private String phone;
	private String refName;
	private String officeName;
	private String officeCode;
	private String companyCode;
	private String companyName;
	private String postCode;
	private String status;
	private String userType;
	private String mgrType;
	private String orderBy;

}
