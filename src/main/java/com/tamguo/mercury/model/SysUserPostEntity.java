package com.tamguo.mercury.model;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * @author tanguo
 */
@Data
@TableName(value="sys_user_post")
public class SysUserPostEntity {

	private String userCode;
	private String postCode;
}
