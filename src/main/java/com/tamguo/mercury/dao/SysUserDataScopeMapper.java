package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysUserDataScopeEntity;

/**
 * @author tanguo
 */
public interface SysUserDataScopeMapper extends SuperMapper<SysUserDataScopeEntity> {

}
