package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysOfficeEntity;
import com.tamguo.mercury.model.condition.SysOfficeCondition;

import java.util.List;

/**
 * @author tanguo
 */
public interface SysOfficeMapper extends SuperMapper<SysOfficeEntity> {

	List<SysOfficeEntity> listData(SysOfficeCondition condition);

}
