package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysUserPostEntity;

/**
 * @author tanguo
 */
public interface SysUserPostMapper extends SuperMapper<SysUserPostEntity> {

}
