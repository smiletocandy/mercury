package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysRoleDataScopeEntity;

/**
 * @author tanguo
 */
public interface SysRoleDataScopeMapper extends SuperMapper<SysRoleDataScopeEntity> {

}
