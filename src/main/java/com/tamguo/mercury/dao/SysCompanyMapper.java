package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysCompanyEntity;
import com.tamguo.mercury.model.condition.SysCompanyCondition;
import java.util.List;

/**
 * @author tanguo
 */
public interface SysCompanyMapper extends SuperMapper<SysCompanyEntity> {

	List<SysCompanyEntity> listData(SysCompanyCondition condition);
	
	SysCompanyEntity selectByCode(String code);

}
