package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysRoleMenuEntity;

/**
 * @author tanguo
 */
public interface SysRoleMenuMapper extends SuperMapper<SysRoleMenuEntity> {

}
