package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysAreaEntity;
import com.tamguo.mercury.model.condition.SysAreaCondition;
import java.util.List;

/**
 * @author tanguo
 */
public interface SysAreaMapper extends SuperMapper<SysAreaEntity> {

	List<SysAreaEntity> listData(SysAreaCondition condition);

}
