package com.tamguo.mercury.dao;

import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysMenuEntity;
import com.tamguo.mercury.model.condition.SysMenuCondition;

import java.util.List;

/**
 * @author tanguo
 */
public interface SysMenuMapper extends SuperMapper<SysMenuEntity> {

	List<SysMenuEntity> listData(SysMenuCondition condition);

	List<SysMenuEntity> selectMenuByRoleId(String roleCode);

}
