package com.tamguo.mercury.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysUserEntity;
import com.tamguo.mercury.model.condition.SysUserCondition;

import java.util.List;

/**
 * @author tanguo
 */
public interface SysUserMapper extends SuperMapper<SysUserEntity> {

	List<SysUserEntity> listData(SysUserCondition condition, Pagination page);

}
