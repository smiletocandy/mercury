package com.tamguo.mercury.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysPostEntity;
import com.tamguo.mercury.model.condition.SysPostCondition;

import java.util.List;

/**
 * @author tanguo
 */
public interface SysPostMapper extends SuperMapper<SysPostEntity> {

	List<SysPostEntity> listData(SysPostCondition condition, Pagination page);

}
