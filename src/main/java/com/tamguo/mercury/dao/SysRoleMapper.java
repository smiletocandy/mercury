package com.tamguo.mercury.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.tamguo.mercury.config.dao.SuperMapper;
import com.tamguo.mercury.model.SysRoleEntity;
import com.tamguo.mercury.model.condition.SysRoleCondition;

import java.util.List;

/**
 * @author tanguo
 */
public interface SysRoleMapper extends SuperMapper<SysRoleEntity> {

	List<SysRoleEntity> listData(SysRoleCondition condition, Page<SysRoleEntity> page);

}
