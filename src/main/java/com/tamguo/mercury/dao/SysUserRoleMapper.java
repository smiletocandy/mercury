package com.tamguo.mercury.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tamguo.mercury.model.SysUserRoleEntity;

/**
 * @author tanguo
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {

}
